﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.

open FSharp.Data
let apikey = System.IO.File.ReadAllText("apikey.txt")
//https://us.api.battle.net/wow/character/wyrmrest-accord/Desfoe?fields=items&locale=en_US&apikey=
type Character = JsonProvider<"Character.json">
type Stats = JsonProvider<"Stats.json">

type ItemSocket =
    | Back
    | Chest
    | Feet
    | Finger1
    | Figner2
    | Hands
    | Head
    | Legs
    | MainHand
    | Neck
    | Shoulder
    | Trinket1
    | Trinket2
    | Waist
    | Wrist

(*
type Item = {Name:string; ILvl:int; Socket:string; ID:int}
let us = ["Desfoe"; "Bodypull"; "Hukenn"; "Rìze"]

let ourStats = us |> List.map (fun person -> (person, Stats.Load("https://us.api.battle.net/wow/character/wyrmrest-accord/"+person+"?fields=stats&locale=en_US&apikey=" + apikey)))

let printStats (stats:Stats.Root) = 
    //printfn "Crit:     %5d %2.2f%%" stats.Stats.CritRating (stats.Stats.Crit) 
    //printfn "Haste:    %5d %2.2f%%" (stats.Stats.HasteRating) (float stats.Stats.HasteRating * 0.00307639327325948458933593525799)
    //printfn "Mastery:  %5d %2.2f%%" stats.Stats.MasteryRating stats.Stats.Mastery
    printfn "Vers:     %5d %2.2f%%" stats.Stats.Versatility (stats.Stats.VersatilityDamageDoneBonus + (stats.Stats.VersatilityDamageTakenBonus + stats.Stats.VersatilityHealingDoneBonus)/decimal 3)
    //()

ourStats |> List.iter (fun (person, stats) -> printfn "%s" person; 
                                              printStats stats)

let getILvl (char:Character.Root) =
    let back = try Some({Name=char.Items.Back.Name;     ILvl=char.Items.Back.ItemLevel;     Socket="Back"; ID=char.Items.Back.Id}) with _-> None
    let chest     = try Some({Name=char.Items.Chest.Name;    ILvl=char.Items.Chest.ItemLevel;    Socket="Chest"; ID=char.Items.Chest.Id}        ) with _->None
    let feet      = try Some({Name=char.Items.Feet.Name;     ILvl=char.Items.Feet.ItemLevel;     Socket="Feet"; ID=char.Items.Feet.Id}         ) with _->None
    let finger1   = try Some({Name=char.Items.Finger1.Name;  ILvl=char.Items.Finger1.ItemLevel;     Socket="Finger1"; ID=char.Items.Finger1.Id}   ) with _->None
    let finger2   = try Some({Name=char.Items.Finger2.Name;  ILvl=char.Items.Finger2.ItemLevel;      Socket="Finger2"; ID=char.Items.Finger2.Id}  ) with _->None
    let hands     = try Some({Name=char.Items.Hands.Name;    ILvl=char.Items.Hands.ItemLevel;     Socket="Hands"; ID=char.Items.Hands.Id}       ) with _->None
    let head      = try Some({Name=char.Items.Head.Name;     ILvl=char.Items.Head.ItemLevel;      Socket="Head"; ID=char.Items.Head.Id}        ) with _->None
    let legs      = try Some({Name=char.Items.Legs.Name;     ILvl=char.Items.Legs.ItemLevel;      Socket="Legs"; ID=char.Items.Legs.Id}        ) with _->None
    let mainhand  = try Some({Name=char.Items.MainHand.Name; ILvl=char.Items.MainHand.ItemLevel;     Socket="MainHand"; ID=char.Items.MainHand.Id} ) with _->None
    let neck      = try Some({Name=char.Items.Neck.Name;     ILvl=char.Items.Neck.ItemLevel;      Socket="Neck"; ID=char.Items.Neck.Id}        ) with _->None
    let shoulder  = try Some({Name=char.Items.Shoulder.Name; ILvl=char.Items.Shoulder.ItemLevel;      Socket="Shoulder"; ID=char.Items.Shoulder.Id}) with _->None
    let trinket1  = try Some({Name=char.Items.Trinket1.Name; ILvl=char.Items.Trinket1.ItemLevel;      Socket="Trinket1"; ID=char.Items.Trinket1.Id}) with _->None
    let trinket2  = try Some({Name=char.Items.Trinket2.Name; ILvl=char.Items.Trinket2.ItemLevel;      Socket="Trinket2"; ID=char.Items.Trinket2.Id}) with _->None
    let waist     = try Some({Name=char.Items.Waist.Name;    ILvl=char.Items.Waist.ItemLevel;      Socket="Waist"; ID=char.Items.Waist.Id}      ) with _->None
    let wrist     = try Some({Name=char.Items.Wrist.Name;    ILvl=char.Items.Wrist.ItemLevel;      Socket="Wrist"; ID=char.Items.Wrist.Id}      ) with _->None
    [ back; chest; feet;finger1;finger2;hands;head;legs;mainhand;neck;shoulder;trinket1;trinket2;waist;wrist] |> List.filter (fun item -> item.IsSome) |> List.map(fun item -> item.Value)
    
*)
[<EntryPoint>]
let main argv = 
(*
    use fout = new System.IO.StreamWriter("output.html")
    let ourILvl = us |> List.map (fun person -> (person, Character.Load("https://us.api.battle.net/wow/character/wyrmrest-accord/"+person+"?fields=items&locale=en_US&apikey=" + apikey))) |> List.map (fun (p, char) -> (p, getILvl char))

    let printPersonLevel ilvl = 
        let printRange items ilvlLimitLow ilvlLimitHigh =
            fprintfn fout "<p>"
            let sub = items |> List.filter (fun item -> item.ILvl >= ilvlLimitLow && item.ILvl < ilvlLimitHigh)
            fprintf fout "<b>%d</b> items with ilvl >= %d < %d" sub.Length ilvlLimitLow ilvlLimitHigh
            sub |> List.iter(fun item -> fprintf fout " <a href='http://www.wowhead.com/item=%d'>%s</a>" item.ID item.Socket)
            fprintfn fout "</p>"

        printRange ilvl 0 840
        printRange ilvl 840 845
        printRange ilvl 845 850
        printRange ilvl 850 999

    fprintf fout "<html><body>"
    ourILvl |> List.iter (fun (person, ilvl) -> fprintfn fout "<h2>%s</h2>" person
                                                printPersonLevel ilvl)
    fprintf fout "</body></html>"
*)
    let what = WebPart.betterItems 134524 "http://www.wowhead.com/items/armor/rings/min-req-level:110/max-req-level:110/class:2/weights:23:123:96:200:215:170/weight-values:100:87:54:40:35:30"
    let sources = what |> List.map (fun item -> match item.Source with
                                                | WebPart.ItemSource.Other (name) -> name
                                                | _ -> "None") |> Set.ofList
    System.Console.ReadLine() |> ignore
    0 // return an integer exit code

