﻿module DataTypes

open FSharp.Data
open System

type Character = JsonProvider< "Character.json" >
type Talents = JsonProvider<"Talents.json">
type Stats = JsonProvider< "Stats.json" >

type ItemSocket = 
    | Back
    | Chest
    | Feet
    | Finger1
    | Finger2
    | Hands
    | Head
    | Legs
    | MainHand
    | Neck
    | Shoulder
    | Trinket1
    | Trinket2
    | Waist
    | Wrist

type Item = 
    { Name : string
      ILvl : int
      Socket : ItemSocket 
      ID : int }

type ItemSource = 
    | RaidMythic of Boss : String * Zone : string
    | RaidHeroic of Boss : String * Zone : string
    | RaidNormal of Boss : String * Zone : string
    | LFR of Boss : String * Zone : String
    | DungeonMythicPlus of Boss : String * Zone : String * level : int
    | DungeonMythic of Boss : String * Zone : String
    | DungeonHeroic of Boss : String * Zone : string
    | DungeonNormal of Boss : String * Zone : string
    | PVP of Rank : String
    | WorldBoss of Name : String
    | Other of String

type WowheadItem = 
    { Name : String
      URL : string
      Score : float
      ID : int
      Source : ItemSource
      ILvl : int
      ScoreDiff : float }

