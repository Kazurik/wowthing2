﻿module WebPart

//Holy Paly
//Head http://www.wowhead.com/items/armor/plate/min-req-level:110/max-req-level:110/class:2/slot:1/weights:23:123:96:200:215:170/weight-values:100:87:54:40:35:30
//Neck http://www.wowhead.com/items/armor/amulets/min-req-level:110/max-req-level:110/class:2/weights:23:123:96:200:215:170/weight-values:100:87:54:40:35:30
//Shoulder http://www.wowhead.com/items/armor/plate/min-req-level:110/max-req-level:110/class:2/slot:3/weights:23:123:96:200:215:170/weight-values:100:87:54:40:35:30
//Cloak: http://www.wowhead.com/items/armor/cloaks/min-req-level:110/max-req-level:110/class:2/weights:23:123:96:200:215:170/weight-values:100:87:54:40:35:30
//Chest: http://www.wowhead.com/items/armor/plate/min-req-level:110/max-req-level:110/class:2/slot:5/weights:23:123:96:200:215:170/weight-values:100:87:54:40:35:30
//Wrist: http://www.wowhead.com/items/armor/plate/min-req-level:110/max-req-level:110/class:2/slot:9/weights:23:123:96:200:215:170/weight-values:100:87:54:40:35:30
//Hands: http://www.wowhead.com/items/armor/plate/min-req-level:110/max-req-level:110/class:2/slot:10/weights:23:123:96:200:215:170/weight-values:100:87:54:40:35:30
//Waist: http://www.wowhead.com/items/armor/plate/min-req-level:110/max-req-level:110/class:2/slot:6/weights:23:123:96:200:215:170/weight-values:100:87:54:40:35:30
//Legs: http://www.wowhead.com/items/armor/plate/min-req-level:110/max-req-level:110/class:2/slot:7/weights:23:123:96:200:215:170/weight-values:100:87:54:40:35:30
//Feet: http://www.wowhead.com/items/armor/plate/min-req-level:110/max-req-level:110/class:2/slot:8/weights:23:123:96:200:215:170/weight-values:100:87:54:40:35:30
//Finger: http://www.wowhead.com/items/armor/rings/min-req-level:110/max-req-level:110/class:2/weights:23:123:96:200:215:170/weight-values:100:87:54:40:35:30
open FSharp.Data
open OpenQA.Selenium
open OpenQA.Selenium.PhantomJS
open OpenQA.Selenium.Support
open System

type ItemSource = 
    | RaidMythic of Boss:String * Zone : string
    | RaidHeroic of  Boss:String * Zone : string
    | RaidNormal of  Boss:String * Zone : string
    | LFR of Boss:String * Zone : String
    | DungeonMythicPlus of Boss:String * Zone : String * level : int
    | DungeonMythic of Boss:String * Zone : String
    | DungeonHeroic of Boss:String * Zone : string
    | DungeonNormal of Boss:String * Zone : string
    | PVP of Rank : String
    | WorldBoss of Name:String
    | Other of String

type WowheadItem = 
    { Name : String
      URL : string
      Score : float
      ID : int
      Source : ItemSource }

let Classes = ["Paladin"; "Rogue"; "Warrior"; "Warlock"; "Mage"; "Priest"; "Monk"; "Shaman"; "Druid"; "Death Knight"; "Demon Hunter"]
let Dungeons = ["Black Rook Hold"; "Court of Stars"; "Darkheart Thicket"; "Eye of Azshara"; "Halls of Valor"; "Maw of Souls"; "Neltharion's Lair"; "The Arcway"; "Vault of the Wardens"; "Violet Hold"]

let betterItems (id : int) (wowheadPage:String) = 
    let rowToItem (row : HtmlNode) = 
        let tds = row.CssSelect("td") |> Array.ofSeq
        let link = tds.[2].CssSelect("a").Head
        let name = link.InnerText()
        let url = row.CssSelect("a").Head.AttributeValue("href")
        
        let id = 
            let regex = new System.Text.RegularExpressions.Regex("item=(\d+)&", Text.RegularExpressions.RegexOptions.Compiled)
            let regMatch = regex.Match(url)
            if regMatch.Success then regMatch.Groups.[1].Value
            else ""

        let source = 
            let td = tds.[2]
            
            let elementExixts (ele:HtmlNode) (by:String) =
                ele.CssSelect(by).Length > 0
            //let rankFound, rank = tryFind (fun () -> td.FindElement(By.CssSelector("span.q2")).Text)
            let rank = 
                let by = "span.q2"
                if elementExixts td by then
                    Some(td.CssSelect(by).Head.InnerText())
                else None
            //let smallSourceFound, smallSource = tryFind (fun () -> td.FindElement(By.XPath(".//div[@class='small2']")).Text)
            let smallSource = 
                //let by = By.XPath(".//div[@class='small2']")
                let by = "div.small2"
                if elementExixts td by then
                    Some(td.CssSelect(by).Head.InnerText())
                else None
            if smallSource.IsSome && smallSource.Value.Contains(" - ") then
                let smallSource = smallSource.Value
                let split = smallSource.Split('-')
                let source = split.[0].Trim()
                let zone = split.[1].Trim()
                if zone = "The Nighthold" || zone = "The Emerald Nightmare" then 
                    if rank.IsSome && rank.Value = "Mythic" then
                        RaidMythic(source, zone)
                    else if rank.IsSome && rank.Value = "Heroic" then
                        RaidHeroic(source, zone)
                    else if rank.IsSome && rank.Value = "Raid Finder" then
                        LFR(source, zone)
                    else
                        RaidNormal(source, zone)
                else if Dungeons |> List.exists ((=) zone) then
                    if rank.IsSome && rank.Value = "Mythic" then
                        DungeonMythic(source, zone)
                    else if rank.IsSome && rank.Value.Contains("Mythic ") then
                        let rank = rank.Value
                        let level = Int32.Parse(rank.Substring(7))
                        DungeonMythicPlus(source, zone, level)
                    else
                        DungeonNormal(source, zone)
                else Other("Unknown - " + smallSource)
            else if smallSource.IsSome && Classes |> List.exists((=) smallSource.Value) then
                Other("Legendary. World Drop")
            else if smallSource.IsSome then
                WorldBoss(smallSource.Value)
            else if rank.IsSome && rank.Value.StartsWith("Legion Season ") then
                PVP(rank.Value)
            else Other("Unknown")

        let score = Double.Parse((tds.[tds.Length - 1]).InnerText())
        { Name = name
          URL = url
          Score = score
          ID = Int32.Parse(id)
          Source = source }
    
    let driver = 
        let pjsOption = new PhantomJSOptions();
        let pjsService = PhantomJSDriverService.CreateDefaultService();
        pjsService.LoadImages <- false;
        new PhantomJSDriver(pjsService, pjsOption)
    driver.Navigate()
        .GoToUrl(wowheadPage)

    let doc = HtmlDocument.Parse(driver.PageSource)
    
    let items = 
        doc.CssSelect("tbody.clickable tr")
        |> Seq.map rowToItem
        |> List.ofSeq
    
    let currentItemScore = 
        try 
            (items |> List.find (fun item -> item.ID = id)).Score
        with _ -> 0.0
    
    items |> List.filter(fun item -> item.Score > currentItemScore)

